# Build *wpa_supplicant* from Source to Mitigate the *PTK0 Rekey Issue*

- Ref: https://gist.github.com/vuori/13e5ab088a882ee8fbf93e64c003e543

## Preparation

1. Clone *wpa_supplicant* source
	- `git clone git://w1.fi/hostap.git`

2. Clone *wpa_supplicant* debian source
	- `git clone https://salsa.debian.org/debian/wpa.git`

3. Copy build config file
	- `cp wpa/debian/config/wpasupplicant/linux hostap/wpa_supplicant/.config`

4. Edit `hostap/wpa_supplicant/config.c`
	- Change line `ssid->wpa_deny_ptk0_rekey = PTK0_REKEY_ALLOW_ALWAYS;` to
	  `ssid->wpa_deny_ptk0_rekey = PTK0_REKEY_ALLOW_NEVER;`

## Dependencies

1. Install dependencies
	```shell
	sudo apt-get install build-essential \
	                     libssl-dev \
						 libpcsclite-dev \
						 libdbus-1-dev \
						 libnl-3-dev \
						 libnl-genl-3-dev \
						 libnl-route-3-dev \
						 libreadline-dev \

## Build

1. Build
	```shell
	cd hostap/wpa_supplicant
	make
	```

## Install

1. Make copy of original binary
	```shell
	sudo mv /sbin/wpa_supplicant /sbin/wpa_supplicant.orig
	sudo cp ./wpa_supplicant /sbin/wpa_supplicant
	```

2. Restart wpa_supplicant related services (e.g., `netplan-wpa-<iface>`)
