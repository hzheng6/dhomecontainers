# Full Disk Encryption (FDE) on Ubuntu 20.04 LTS with TPM2 Auto Decryption on Boot
- Tested on *desktop* version, but most of these should also applies to *server* version

## Enable FDE (Desktop Installer)

### Create Encrypted Partitions

1. In *Installation Type*, choose *something else*
2. Allocate the following partitions
	1. EFI partition:
		- size: >= 512 MB (e.g., 1024 MB)
		- type: EFI
		- encryption: no
	2. Boot partition:
		- size: >= 512 MB (e.g., 1024 MB)
		- type: ext4
		- encryption: no
		- mount point: `/boot`
	3. Root partition:
		- size: as needed
		- type: choose *physical encrypted volume*
		- enter passphrase
		- after the volume is created, one `<DEV_NAME>_crypt` will appear
		- change its mount point to `/`
	4. Home partition (optional):
		- similar process as the Root partition
		- change its mount point to `/home`
3. Choose boot device to the EFI partition
4. Click *Install Now*
5. If a window appear saying something like "failed to mount `<DEV_NAME>_crypt` to root `/`
	1. Open a terminal
	2. format the encrypted partition with the following commands:
		- `mkfs -t ext4 /dev/mapper/<DEV_NAME>_crypt`
	3. use system monitor to kill the installer *ubiquity*
	4. restart the installer
	5. when configuring the partition, do not redo the process, instead,
	   correct all mount points, and check the *format partition* box if possible on all partitions we just created
	6. now we can proceed to install

### Configure Manual Decryption on Boot

reference: https://vitobotta.com/2018/01/11/ubuntu-full-disk-encryption-manual-partitioning-uefi/

1. Get the UUID of the encrypted volume

	`blkid /dev/<DEV_NAME>`

2. Configure `/etc/crypttab`

	`echo '<DEV_NAME>_crypt UUID=(the uuid without quotes) none luks,discard,initramfs' > /target/etc/crypttab`

	- NOTE: append to the file if there're multiple encrypted partitions

3. Re-mount `/boot`, `/boot/efi`, `/home` if they have been un-mounted by the installer

	```
	mount -t ext4 /dev/<DEV_NAME> /target/boot
	mount -t vfat /dev/<DEV_NAME> /target/boot/efi
	mount -t ext4 /dev/mapper/<DEV_NAME>_crypt /target/home
	```

4. chroot into the new installation

	```
	mount -t proc proc /target/proc
	mount --rbind /sys /target/sys
	mount --rbind /dev /target/dev
	chroot /target
	```

5. Update GRUB and *initramfs*

	```
	grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader=ubuntu --boot-directory=/boot/efi/EFI/ubuntu --recheck --uefi-secure-boot /dev/<EFI_DEV_NAME>
	grub-mkconfig --output=/boot/efi/EFI/ubuntu/grub/grub.cfg
	update-initramfs -u -k all
	exit
	reboot
	```

## Configure Auto Decryption with TPM2

- In future Ubuntu release, there might be `systemd-cryptenroll` to enroll LUKS keys to TPM2
- Currently, we're using `clevis` for this job

1. Install Clevis

	reference: https://askubuntu.com/questions/1328189/ubuntu-20-04-autoinstaller-uefi-full-disk-encryption

	```
	sudo apt update
	sudo apt upgrade
	sudo apt install clevis-luks clevis-tpm2 clevis-initramfs
	```

2. Bind Drives to Clevis

	- reference: https://security.stackexchange.com/questions/194081/use-tpm2-0-to-securely-decrypt-the-hard-drive-in-linux-unattended
	- reference: https://www.mankier.com/1/clevis-luks-bind
	- reference for PCR registers: https://man7.org/linux/man-pages/man1/systemd-cryptenroll.1.html

	```
	clevis luks bind -d /dev/<DEV_NAME> tpm2 '{"pcr_ids":"0,7"}'
	```

3. Configure `/etc/default/grub` to disable graphical boot screen, which may cause the boot process stuck on *askpass* screen

	reference: https://ubuntuhandbook.org/index.php/2020/05/boot-ubuntu-20-04-command-console/

	1. disable `GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"` by adding `#` at the beginning
	2. set `GRUB_CMDLINE_LINUX=""` to `GRUB_CMDLINE_LINUX="text"`

4. Configure `/etc/default/grub` to shorten the timeout

	1. set `GRUB_TIMEOUT`
	2. set `GRUB_RECORDFAIL_TIMEOUT`

5. Fix Clevis's *initramfs* script

	1. The issue is described in https://github.com/latchset/clevis/issues/277
	2. The fix is available at https://github.com/latchset/clevis/pull/293
	3. Find the `clevis` script under either `/etc/initramfs-tools/scripts/local-top/` or `/usr/share/initramfs-tools/scripts/local-top/`
	4. Fix the issue if it's still exist in the current release

6. Update *initramfs* and GRUB

	```
	grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader=ubuntu --boot-directory=/boot/efi/EFI/ubuntu --recheck --uefi-secure-boot /dev/<EFI_DEV_NAME>
	grub-mkconfig --output=/boot/efi/EFI/ubuntu/grub/grub.cfg
	update-initramfs -u -k all
	```

7. Reboot
