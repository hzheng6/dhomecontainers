# Router with Ubuntu 20.04 LTS

## Configure Netplan

1. Edit the netplan config file under `/etc/netplan/*.yaml`

2. Example configuration:

	```yaml
	network:
		version: 2
		renderer: networkd

		ethernets:
			eno1:
				optional: true
				dhcp4: no
				addresses:
				- 10.101.0.1/16
				dhcp4: false
				nameservers:
					addresses:
					- 8.8.8.8
					- 8.8.4.4

		wifis:
			wlp0s20f3:
				optional: true
				dhcp4: yes
				dhcp-identifier: mac
				access-points:

					"WIFI_SSID_1":
						password: "WAP2_PASSCODE"

					"WIFI_SSID_2":
					auth:
						key-management: eap
						method: peap
						ca-certificate: /path/to/cert.pem
						identity: "USERID"
						password: "PASSWORD"
	```

	- Note: for Ubuntu desktop, ensure to change renderer from *NetworkManager* to *networkd*

3. For Ubuntu desktop, stop & disable *NetworkManager*

	```
	sudo systemctl stop NetworkManager
	sudo systemctl disable NetworkManager
	```

4. Enable Wi-Fi adaptor if necessary

	```
	sudo ip link set wlp0s20f3 up
	```

5. Apply Netplan setting

	```
	sudo netplan generate
	sudo netplan apply
	```

## Setup DHCP server

Reference: https://medium.com/@exesse/how-to-make-a-simple-router-gateway-from-ubuntu-server-18-04-lts-fd40b7bfec9

1. Install DHCP server

	```
	sudo apt update
	sudo apt install isc-dhcp-server
	```

2. Edit `/etc/default/isc-dhcp-server`

	- change `INTERFACESv4=""` to `INTERFACESv4="<INTF_ID>"`
	- change `INTERFACESv6=""` to `INTERFACESv6="<INTF_ID>"` (optional)

3. Edit `/etc/dhcp/dhcpd.conf`

	```
	subnet 10.101.0.0 netmask 255.255.0.0 {
		range 10.101.0.2 10.101.0.254;
		option domain-name-servers 8.8.4.4, 8.8.8.8;
		option subnet-mask 255.255.0.0;
		option broadcast-address 10.101.0.255;
		option routers 10.101.0.1;
		default-lease-time 600;
		max-lease-time 7200;
	}
	```

	- Note: where `option routers 10.101.0.1;` is the address of the router
	- DHCP server will fail to start if the address above is not assigned to the interface

4. Edit `/etc/dhcp/dhcpd6.conf` if necessary

5. Edit `/lib/systemd/system/isc-dhcp-server.service`

	- Add following line under `[Service]` block

		```
		Restart=on-failure
		RestartSec=10s
		```

	- So that the DHCP server will restart if failed at boot

6. Edit `/lib/systemd/system/isc-dhcp-server6.service` similarly, if necessary

## Configure NAT forward

1. Enable IPv4 forwarding

	```
	sudo sysctl -w net.ipv4.ip_forward=1

	# verify result with
	sysctl net.ipv4.ip_forward
	```

2. Edit `/etc/sysctl.conf`

	- Uncomment/add line `net.ipv4.ip_forward=1`

3. Configure UFW rules

	1. Edit `/etc/default/ufw`
		- Change line `DEFAULT_FORWARD_POLICY=...` to `DEFAULT_FORWARD_POLICY="ACCEPT"`

	2. Edit `/etc/ufw/before.rules`; add following lines before `*filter` rules

		```
		# NAT table rules
		*nat
		:POSTROUTING ACCEPT [0:0]

		# Forward traffic through the interface specified
		-A POSTROUTING -s 10.101.0.0/16 -o <OUTGOING_NET_INTF> -j MASQUERADE

		# don't delete the 'COMMIT' line or these nat table rules won't be processed
		COMMIT
		```

	3. Reboot UFW

		```
		sudo ufw disable
		sudo ufw enable
		```

	4. Allow necessary ports

		```sh
		# SSH Server
		sudo ufw allow 22/tcp

		# DHCP Server
		sudo ufw allow proto udp from 10.101.0.0/16 to any port 67

		# Multi-cast
		sudo ufw allow from 10.101.0.0/16 to 224.0.0.0/4
		sudo ufw allow from 10.101.0.0/16 to 239.0.0.0/8

		# Unifi Controller ports
		# Ref: https://help.ui.com/hc/en-us/articles/218506997-UniFi-Ports-Used
		sudo ufw allow proto udp from 10.101.0.0/16 to any port 3478
		sudo ufw allow proto udp from 10.101.0.0/16 to any port 5514
		sudo ufw allow proto tcp from 10.101.0.0/16 to any port 8080
		sudo ufw allow proto tcp from 10.101.0.0/16 to any port 8443
		sudo ufw allow proto tcp from 10.101.0.0/16 to any port 8880
		sudo ufw allow proto tcp from 10.101.0.0/16 to any port 8843
		sudo ufw allow proto tcp from 10.101.0.0/16 to any port 6789
		sudo ufw allow proto udp from 10.101.0.0/16 to any port 10001
		sudo ufw allow proto udp from 10.101.0.0/16 to any port 1900

		```

## Other Configs

- Region code for Wi-Fi device
	1. Set default region code with `sudo iw reg set <2_CHAR_REG_CODE>`
	2. Edit file `/etc/default/crda` to fill in the line `REGDOMAIN=`

- Unifi Access Points
	- Security protocol (WPA2/WPA3...)
		- Rekeying interval
	- *None* on DHCP

- Unifi with Let's Encrypt certificate

	```shell
	if [ [ $RENEWED_DOMAINS = "<DOMAIN_NAME>" ] ]; then
		openssl pkcs12 \
				-export \
				-keypbe aes-256-cbc \
				-in /etc/letsencrypt/live/<DOMAIN_NAME>/cert.pem \
				-inkey /etc/letsencrypt/live/<DOMAIN_NAME>/privkey.pem \
				-CAfile /etc/letsencrypt/live/<DOMAIN_NAME>/chain.pem \
				-caname "R3" \
				-caname "ISRG Root X1" \
				-out /usr/lib/unifi/data/unifi.p12 \
				-name unifi \
				-passout pass:aircontrolenterprise

		keytool -importkeystore \
				-deststorepass aircontrolenterprise \
				-destkeypass aircontrolenterprise \
				-destkeystore /usr/lib/unifi/data/keystore \
				-srckeystore /usr/lib/unifi/data/unifi.p12 \
				-srcstoretype PKCS12 \
				-srcstorepass aircontrolenterprise \
				-alias unifi \
				-noprompt -v

		systemctl restart unifi
	fi
	```
