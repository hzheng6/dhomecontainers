# DHome Infra Base Docker Container

## Build

```
docker build --build-arg CLOUD_REGION=${!CLOUD_REGION!} -t zhenghaven/dhome-infra-base ./DHomeInfraBase/src/
```

- Variables to be plugged-in
	- ${!CLOUD_REGION!} - The name of the cloud region

## Run

```
docker run --tty --interactive \
	--add-host=host.docker.internal:host-gateway \
	--restart=always \
	--name <DHomeInfraBase> \
	--hostname <DHomeInfraBase> \
	zhenghaven/dhome-infra-base
```

- Variables to be plugged-in
	- <DHomeInfraBase> - The name of the container
	- <DHomeInfraBase> - The name of the host
