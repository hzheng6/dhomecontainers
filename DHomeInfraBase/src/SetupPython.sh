#! /bin/bash

UBUNTU_REPO_ADDR=""

if [[ $CLOUD_REGION == "DEFAULT" ]]
then
	echo "Default cloud region"
	UBUNTU_REPO_ADDR="archive.ubuntu.com"
elif [[ $CLOUD_REGION == "AWS_US_WEST_1" ]]
then
	echo "AWS US West 1 cloud region"
	UBUNTU_REPO_ADDR="us-west-1.ec2.archive.ubuntu.com"
elif [[ $CLOUD_REGION == "AZURE" ]]
then
	echo "Azure cloud region"
	UBUNTU_REPO_ADDR="azure.archive.ubuntu.com"
elif [[ $CLOUD_REGION == "GENERAL_CN" ]]
then
	echo "General CN cloud region"
	UBUNTU_REPO_ADDR="mirrors.tuna.tsinghua.edu.cn"
elif [[ $CLOUD_REGION == "TENCENT_CN" ]]
then
	echo "Tencent CN cloud region"
	UBUNTU_REPO_ADDR="mirrors.tencentyun.com"
else
	echo "Invalid CLOUD_REGION value"
	exit -1
fi

sed -i "s/archive.ubuntu.com/$UBUNTU_REPO_ADDR/" /etc/apt/sources.list

apt-get update -y
apt-get install -y git python3 python3-pip
