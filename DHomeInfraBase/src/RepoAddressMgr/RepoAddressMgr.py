#! /bin/python3

import os
import sys
import json
import argparse

THIS_PY_SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
DEFAULT_CONFIG_FILE_PATH = os.path.join(THIS_PY_SCRIPT_DIR, 'RepoAddressConfig.json')

def main():
	parser = argparse.ArgumentParser(description='RepoAddressMgr')
	parser.add_argument('name', type=str, help='Name of the address')
	parser.add_argument('--config', type=str, help='Path to the config file.', default=None, required=False)
	parser.add_argument('--region', type=str, help='Name of the cloud region to be queried.', default=os.environ['CLOUD_REGION'], required=False)
	args = parser.parse_args()

	if args.region is None:
		sys.stderr.write('The cloud region is not specified\n')
		return -1

	configPath = DEFAULT_CONFIG_FILE_PATH if args.config is None else os.path.realpath(os.path.abspath(args.config))
	config = None
	with open(configPath, 'r') as configFile:
		config = json.loads(configFile.read())

	if args.region not in config['Addresses']:
		sys.stderr.write('The cloud region specified is not in config file\n')
		return -1

	defaultAddrs = config['Addresses'][config['Default']]

	resAddr = defaultAddrs[args.name]
	if args.name in config['Addresses'][args.region]:
		resAddr = config['Addresses'][args.region][args.name]

	sys.stdout.write('{}'.format(
		resAddr
	))

	return 0

if __name__ == '__main__':
	exit(main())

