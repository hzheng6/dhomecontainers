# Encrypted DNS Docker Container

## Build

```
docker build -t zhenghaven/router-dns ./RouterDns/src/
```

## Setup

1. Create log directory
```
mkdir -p /var/log/DHomeInfra/RouterDns
chmod 755 /var/log/DHomeInfra/RouterDns
```

2. Add a user defined network interface
  (so that we can assigned static IP to DNS service)

```shell
docker network create --ipv6 --driver=bridge \
	--subnet=172.117.0.0/16 \
	--gateway=172.117.0.1 \
	--subnet=fd00:117::/64 \
	server_net
```

## Run

```
docker run --tty --interactive \
	--add-host=host.docker.internal:host-gateway \
	--volume /var/log/DHomeInfra/RouterDns:/dockermnt/log \
	--restart=unless-stopped \
	--net server_net \
	--ip 172.117.8.8 \
	--name <RouterDns> \
	--hostname <RouterDns> \
	zhenghaven/router-dns
```

- Variables to be plugged-in
	- `--net`      - The name of the user defined network interface
	- `--ip`       - The static IP that we want to assign to the DNS service
	- `--name`     - The name of the container
	- `--hostname` - The name of the host
