#! /bin/bash

SWAP_FILE_PATH="/swapfile"
SWAP_SIZE_IN_MB=1024

if [ $(whoami) != 'root' ]; then
	echo "ERROR: root priviledge is needed for this script."
	exit 1
fi

if [ ! -f "${SWAP_FILE_PATH}" ]; then
	echo "Creating swap file..."
	fallocate -l 1G "${SWAP_FILE_PATH}"
fi

if [ -z "$(swapon -s)" ]; then
	echo "Loading swap file..."
	dd if=/dev/zero of="${SWAP_FILE_PATH}" bs=1M count=${SWAP_SIZE_IN_MB}
	chmod 600 "${SWAP_FILE_PATH}"
	sudo mkswap "${SWAP_FILE_PATH}"
	sudo swapon "${SWAP_FILE_PATH}"
fi

if [ -z "$(grep "swap" /etc/fstab)" ]; then
	echo "Adding swap file to fstab..."
	echo "${SWAP_FILE_PATH} swap swap defaults 0 0" >> /etc/fstab
fi
