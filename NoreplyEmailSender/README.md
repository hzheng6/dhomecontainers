# Noreply Email Sender Docker Container

## Build

```
docker build -t zhenghaven/noreply-email-sender ./NoreplyEmailSender/src/
```

## Setup

1. Create directory for certificates and keys
```
mkdir -p /etc/DHomeInfra/NoreplyEmailSender/letsencrypt/:domain:
chmod 700 /etc/DHomeInfra/NoreplyEmailSender/letsencrypt/:domain:
mkdir -p /etc/DHomeInfra/NoreplyEmailSender/dkim
chmod 700 /etc/DHomeInfra/NoreplyEmailSender/dkim
```

2. Create hard links
```
ln /etc/dkim/:domain:/key.pem /etc/DHomeInfra/NoreplyEmailSender/dkim/key.pem
```

3. Create deploy hook to letsencrypt
	1. copy `a_DeployNoreplyEmailSenderCerts.py` to `/etc/letsencrypt/renewal-hooks/deploy/`
	2. fill-in `a_DeployNoreplyEmailSenderCerts.py` if necessary
	3. run `a_DeployNoreplyEmailSenderCerts.py` once

4. Create log directory
```
mkdir -p /var/log/DHomeInfra/NoreplyEmailSender
chmod 755 /var/log/DHomeInfra/NoreplyEmailSender
```

5. Enable IPv6 NAT on Docker daemon (if IPv6 is available at host)

- Add the following line to `/etc/docker/daemon.json` (create one if not exist).
```json
{
	"ipv6": true,
	"fixed-cidr-v6": "fd00:17::/64",
	"experimental": true,
	"ip6tables": true
}
```
- Restart the docker daemon

## Run

```
docker run --tty --interactive \
	--add-host=host.docker.internal:host-gateway \
	--volume /var/log/DHomeInfra/NoreplyEmailSender:/dockermnt/log \
	--volume /etc/DHomeInfra/NoreplyEmailSender/letsencrypt:/dockermnt/letsencrypt \
	--volume /etc/DHomeInfra/NoreplyEmailSender/dkim:/dockermnt/dkim \
	-p 50443:443/tcp \
	--restart=unless-stopped \
	--name <NoreplyEmailSender> \
	--hostname <NoreplyEmailSender> \
	zhenghaven/noreply-email-sender
```

- Variables to be plugged-in
	- `--name`     - The name of the container
	- `--hostname` - The name of the host
