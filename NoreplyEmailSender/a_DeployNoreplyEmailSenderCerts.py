#!/bin/python3

import os
import shutil

########## Fill in this part ##########

DEPLOYED_CERT_DIR = os.path.abspath('')
DOMAINS_IN_INTERESTS = []

#######################################


UPDATED_CERT_DIR = os.environ['RENEWED_LINEAGE']
RENEWED_DOMAIN = os.path.basename(UPDATED_CERT_DIR)

FILES_TO_DEPLOY = ['fullchain.pem', 'privkey.pem']

if RENEWED_DOMAIN in DOMAINS_IN_INTERESTS:
	srcDir = UPDATED_CERT_DIR
	dstDir = os.path.join(DEPLOYED_CERT_DIR, RENEWED_DOMAIN)

	for item in FILES_TO_DEPLOY:
		srcPath = os.path.join(srcDir, item)
		dstPath = os.path.join(dstDir, item)
		shutil.copy2(srcPath, dstPath)
