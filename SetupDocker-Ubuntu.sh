#! /bin/bash

DOCKER_FINGERPRINT="9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88"

trim_lead()
{
	echo -e "${1}" | sed -e 's/^[[:space:]]*//'
}

trim_trail()
{
	echo -e "${1}" | sed -e 's/[[:space:]]*$//'
}

trim()
{
	rs1=$(trim_lead "$1")

	trim_trail "$rs1"
}

if [ $(whoami) != 'root' ]; then
	echo "ERROR: root priviledge is needed for this script."
	exit 1
fi

if [ -z "${CLOUD_REGION}" ]; then
	echo "Environment variable CLOUD_REGION must be set."
	exit 1
fi

if [ "${CLOUD_REGION}" = "TENCENT_CN" ]; then
	echo "Using Docker repo from tencent.com..."
	export REPO_MIRROR_DOCKER_CE="http://mirrors.cloud.tencent.com/docker-ce/linux/ubuntu"
elif [ "${CLOUD_REGION}" = "GENERAL_CN" ]; then
	echo "Using Docker repo from tsinghua.edu.cn..."
	export REPO_MIRROR_DOCKER_CE="http://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/ubuntu"
else
	echo "Using Docker repo from docker.com..."
	export REPO_MIRROR_DOCKER_CE="http://download.docker.com/linux/ubuntu"
fi

apt-get remove -y docker docker-engine docker.io containerd runc

apt-get update

apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

dockFingerprint="$(apt-key fingerprint 0EBFCD88 | grep 9DC8)"
dockFingerprint="$(trim "$dockFingerprint")"

if [ "$dockFingerprint" != "$DOCKER_FINGERPRINT" ]; then
	echo "ERROR: Fingerprint added for docker repo doesn\'t match value expected."
	echo "Added $dockFingerprint"
	echo "Expected $DOCKER_FINGERPRINT"
	exit 1
fi

add-apt-repository \
   "deb [arch=amd64] ${REPO_MIRROR_DOCKER_CE} \
   $(lsb_release -cs) \
   stable"

apt-get update
apt-get install docker-ce docker-ce-cli containerd.io
